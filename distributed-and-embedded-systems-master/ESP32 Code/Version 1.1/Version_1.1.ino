#include<DHT.h>

//Definitions for DHT11
#define DHT_Pin  2       
#define DHT_Type DHT11

//Definitions for Button
#define BUTTON_Pin 33   

//Definitions for RGB
#define R_Pin   27      //Pins for RGB Led
#define B_Pin   26
#define G_Pin   25
#define RGB_Type CC    
  #define ON   LOW
  #define OFF  HIGH

//Definitions for Photoresistor
#define LDR_Pin 
#define MAX_ADC_Reading 4096
#define ADC_Ref_Voltage 3.3
#define LUX_Scalar 12518931
#define LUX_Exponent -1.405 
#define REF_Resistance 10000

DHT dht11(DHT_Pin, DHT_Type);

volatile float humidity[60], temp[60], ldrLux[60];            //Containers to store obtained readings
float raw, resistorVoltage, ldrVoltage, ldrResistance;
unsigned long int printTime, readTime;
int currButtonTime;
int interval=10;
int intervalArr[]={5,10,30,60,120,300};                 //All available intervals are stored in an array
int intervalPtr=1, dataIndex=0, maxIndex=2;             
int currH_ledStatus=0 ,prevH_ledStatus=0,currT_ledStatus=0 ,prevT_ledStatus=0,currL_ledStatus=0 ,prevL_ledStatus=0;                 //0-Green, 1-Amber, 2-Red

/*void intervalConfig()
{ 
  maxIndex = interval/5; 
  dataIndex = 0;
  
  Serial.print("Output interval is now ");
  if(interval==120||interval==300)
  {
    Serial.print(interval/60);
    Serial.println("min");   
  }
  else
  {
    Serial.print(interval);
    Serial.println("sec");   
  }
}
*/
void setup() {  
  Serial.begin(115200);
  Serial.println("System Initialising...");
  dht11.begin();
  pinMode(R_Pin, OUTPUT);
  pinMode(G_Pin, OUTPUT);
  pinMode(B_Pin, OUTPUT);
  pinMode(BUTTON_Pin, INPUT);
  readTime = millis();            //To make start point of read and print interval same
  printTime = millis();           //To check print interval
  digitalWrite(R_Pin, OFF);
  digitalWrite(G_Pin, ON);
  digitalWrite(B_Pin, OFF);

}

void loop() {
  if((millis()-readTime)>5000)            //Reads Data at 5s interval
  {
    readSensors(); 
    readTime = millis();                  
  }
  if((millis()-printTime)>(interval*1000)) //Prints Data at specified interval
  {
    printData();
    printTime = millis(); 
  }
  if(digitalRead(BUTTON_Pin)==LOW)
  {
    currButtonTime = millis();
    while(digitalRead(BUTTON_Pin)==LOW)
    {
      if((currButtonTime-millis())>1000)
      {
        intervalPtr++;
        interval=intervalArr[intervalPtr];
        if(intervalPtr==6) intervalPtr=0;
      
        while(digitalRead(BUTTON_Pin)==LOW);
        break;
      }
    }
  }
}

void readSensors()
{
  //Humidity Read and Status Check
  humidity[dataIndex] = dht11.readHumidity();
  if((humidity[dataIndex]>=50)&&(humidity[dataIndex]<=70))
    currH_ledStatus=0;
  else if((humidity[dataIndex]>=30)&&(humidity[dataIndex]<50)||(humidity[dataIndex]>70)&&(humidity[dataIndex]<=85))
    currH_ledStatus=1;
  else if((humidity[dataIndex]<30)||(humidity[dataIndex]>85))
    currH_ledStatus=2;
  if(ledStatusCheck(currH_ledStatus,prevH_ledStatus))
  {
    changeLed(currH_ledStatus);
    Serial.println("\nHumidity in area has changed.");
  }
  delay(100);
  prevH_ledStatus = currH_ledStatus;

  //Temperature Read and Status Check
  temp[dataIndex] = dht11.readTemperature();
  if((temp[dataIndex]>=20)&&(temp[dataIndex]<=25))
      currT_ledStatus=0;
  else if((temp[dataIndex]>=18)&&(temp[dataIndex]<20)||(temp[dataIndex]>25)&&(temp[dataIndex]<=27))
    currT_ledStatus=1;
  else if((temp[dataIndex]<18)||(temp[dataIndex]>27))
    currT_ledStatus=2;
  if(ledStatusCheck(currT_ledStatus,prevT_ledStatus))
  {
    changeLed(currT_ledStatus);
    Serial.println("\nTemparture in area has changed.");
  }
  delay(100);
  prevT_ledStatus = currT_ledStatus;

  //Intesity Read and Status Check
  raw = analogRead(LDR_Pin);
  resistorVoltage = (float)raw / MAX_ADC_Reading * ADC_Ref_Voltage;
  ldrVoltage = ADC_Ref_Voltage - resistorVoltage;
  ldrResistance = ldrVoltage/resistorVoltage * REF_Resistance;        // REF_Resistance is 5 kohm
  ldrLux[dataIndex] = LUX_Scalar * pow(ldrResistance, LUX_Exponent);

  if((ldrLux[dataIndex]>=25000)&&(ldrLux[dataIndex]<=50000))
    currL_ledStatus=0;
  else if((ldrLux[dataIndex]>=20000)&&(ldrLux[dataIndex]<25000)||(ldrLux[dataIndex]>50000)&&(ldrLux[dataIndex]<=75000))
    currL_ledStatus=1;
  else if((ldrLux[dataIndex]<20000)||(ldrLux[dataIndex]>75000))
    currL_ledStatus=2;
  if(ledStatusCheck(currL_ledStatus,prevL_ledStatus))
  {
    changeLed(currL_ledStatus);
    Serial.println("\nLight  has changed.");
  }
  delay(100);
  prevL_ledStatus = currL_ledStatus; 
  
  dataIndex++;
  if(dataIndex==maxIndex)
  {
    dataIndex=0;
  }
}

void printData()
{
  for(int printIndex = 0; printIndex < maxIndex ; printIndex++)
  {
    Serial.print("Humidity: ");
    Serial.print(humidity[printIndex]);
    Serial.print(" %\t");
    Serial.print("Temperature: ");
    Serial.print(temp[printIndex]);
    Serial.print(" *C ");  
    Serial.print("Lux: ");
    Serial.print(ldrLux[printIndex]);
    Serial.println(" lux "); 
  }
}

void changeLed(int ledStatus)
{
  if(ledStatus==0)
  {
    digitalWrite(R_Pin, OFF);
    digitalWrite(G_Pin, ON);
    digitalWrite(B_Pin, OFF);
  }
  else if(ledStatus==1)
  {
    digitalWrite(R_Pin, OFF);
    digitalWrite(G_Pin, OFF);
    digitalWrite(B_Pin, ON);
  }
  else if(ledStatus==2)
  {
    digitalWrite(R_Pin, ON);
    digitalWrite(G_Pin, OFF);
    digitalWrite(B_Pin, OFF);
  }
}

int ledStatusCheck(int curr_ledStatus, int prev_ledStatus)
{
  if(curr_ledStatus != prev_ledStatus)
  {
    if(prev_ledStatus==0 || curr_ledStatus==0)
    {
      return 1;
    }
  }
  return 0;
}
