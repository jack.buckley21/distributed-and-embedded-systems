#include<DHT.h>
#include<Wire.h>
#include<Adafruit_Sensor.h>
#include<Adafruit_TSL2561_U.h>

//Definitions for DHT11
#define DHT_Pin  2       //DHT11 Pin Number
#define DHT_Type DHT11

//Definitions for Button
#define BUTTON_Pin 4    
#define MAX_DEBOUNCE //50000

//Definitions for RGB
#define R_Pin   5     //Pins for RGB Led
#define G_Pin   18
#define B_Pin   19
#define RGB_Type CC     
  #define ON   HIGH
  #define OFF  LOW


//Definitions for TSL2561
Adafruit_TSL2561_Unified lightSensor = Adafruit_TSL2561_Unified(TSL2561_ADDR_FLOAT, 12345);

DHT dht11(DHT_Pin, DHT_Type);

volatile float humidity[60], temp[60], lightLux[60];            //to store  readings
unsigned long int printTime, readTime;
unsigned long int debounce;
int interval=10;
int intervalArr[]={5,10,30,60,120,300};                 //All available intervals are stored in an array
int intervalPtr=1, dataIndex=0, maxIndex=2;             
int currH_ledStatus=0 ,prevH_ledStatus=0,currT_ledStatus=0 ,prevT_ledStatus=0,currL_ledStatus=0 ,prevL_ledStatus=0;                 //0-Green, 1-Amber, 2-Red

void selfSensorTest();
void readSensors();
void printData();
void configureSensor();

void intervalConfig()
{ 
  maxIndex = interval/5; 
  dataIndex = 0;
  
  Serial.print("Output interval is now ");
  if(interval==120||interval==300)
  {
    Serial.print(interval/60);
    Serial.println("min");   
  }
  else
  {
    Serial.print(interval);
    Serial.println("sec");   
  }
}

void setup() {  
  Serial.begin(115200);
  Serial.println("System Initialising..");
  dht11.begin();
  selfSensorTest();
  configureSensor();
  pinMode(R_Pin, OUTPUT);
  pinMode(G_Pin, OUTPUT);
  pinMode(B_Pin, OUTPUT);
  pinMode(BUTTON_Pin, INPUT_PULLUP);
  readTime = millis();            //To make start point of read and print interval same
  printTime = millis();           //To check print interval
  digitalWrite(R_Pin, OFF);
  digitalWrite(G_Pin, ON);
  digitalWrite(B_Pin, OFF);
  intervalConfig();
}

void loop() {
  if((millis()-readTime)>5000)            //Reads Data at basic rate
  {
    readSensors(); 
    readTime = millis();                  
  }
  if((millis()-printTime)>(interval*1000)) //Prints Data at specified interval
  {
    printData();
    printTime = millis(); 
  }
  if(digitalRead(BUTTON_Pin)==LOW)
  {
    debounce++;
      if(debounce>MAX_DEBOUNCE)
      {
        intervalPtr++;
        if(intervalPtr==6) intervalPtr=0;
        interval=intervalArr[intervalPtr];
        intervalConfig();
        debounce=0;
      }
  }
}

void readSensors()
{
  //Humidity Read and Status Check
  humidity[dataIndex] = dht11.readHumidity();
  if((humidity[dataIndex]>=50)&&(humidity[dataIndex]<=70))
    currH_ledStatus=0;
  else if((humidity[dataIndex]>=30)&&(humidity[dataIndex]<50)||(humidity[dataIndex]>70)&&(humidity[dataIndex]<=85))
    currH_ledStatus=1;
  else if((humidity[dataIndex]<30)||(humidity[dataIndex]>85))
    currH_ledStatus=2;
  if(ledStatusCheck(currH_ledStatus,prevH_ledStatus))
  {
    changeLed(currH_ledStatus);
    Serial.println("\nHumidity   changed.");
  }
  delay(100);
  prevH_ledStatus = currH_ledStatus;

  //Temperature Read and Status Check
  temp[dataIndex] = dht11.readTemperature();
  if((temp[dataIndex]>=20)&&(temp[dataIndex]<=25))
      currT_ledStatus=0;
  else if((temp[dataIndex]>=18)&&(temp[dataIndex]<20)||(temp[dataIndex]>25)&&(temp[dataIndex]<=27))
    currT_ledStatus=1;
  else if((temp[dataIndex]<18)||(temp[dataIndex]>27))
    currT_ledStatus=2;
  if(ledStatusCheck(currT_ledStatus,prevT_ledStatus))
  {
    changeLed(currT_ledStatus);
    Serial.println("\nTemperature   changed.");
  }
  delay(100);
  prevT_ledStatus = currT_ledStatus;

  //liight Read and Status Check
  sensors_event_t event;
  lightSensor.getEvent(&event);
  lightLux[dataIndex]= event.light;

  if((lightLux[dataIndex]>=25000)&&(lightLux[dataIndex]<=50000))
    currL_ledStatus=0;
  else if((lightLux[dataIndex]>=20000)&&(lightLux[dataIndex]<25000)||(lightLux[dataIndex]>50000)&&(lightLux[dataIndex]<=75000))
    currL_ledStatus=1;
  else if((lightLux[dataIndex]<20000)||(lightLux[dataIndex]>75000))
    currL_ledStatus=2;
  if(ledStatusCheck(currL_ledStatus,prevL_ledStatus))
  {
    changeLed(currL_ledStatus);
    Serial.println("\nLight Intensity  changed.");
  }
  delay(100);
  prevL_ledStatus = currL_ledStatus; 
  
  dataIndex++;
  if(dataIndex==maxIndex)
  {
    dataIndex=0;
  }
}

void printData()
{
  for(int printIndex = 0; printIndex < maxIndex ; printIndex++)
  {
    Serial.print("Humidity: ");
    Serial.print(humidity[printIndex]);
    Serial.print(" %\t");
    Serial.print("Temperature: ");
    Serial.print(temp[printIndex]);
    Serial.print(" *C\t ");  
    Serial.print("Lux: ");
    Serial.print(lightLux[printIndex]);
    Serial.println(" lux "); 
  }
}

void changeLed(int ledStatus)
{
  if(ledStatus==0)
  {
    digitalWrite(R_Pin, OFF);
    digitalWrite(G_Pin, ON);
    digitalWrite(B_Pin, OFF);
  }
  else if(ledStatus==1)
  {
    digitalWrite(R_Pin, ON);
    digitalWrite(G_Pin, ON);
    digitalWrite(B_Pin, OFF);
  }
  else if(ledStatus==2)
  {
    digitalWrite(R_Pin, ON);
    digitalWrite(G_Pin, OFF);
    digitalWrite(B_Pin, OFF);
  }
}

int ledStatusCheck(int curr_ledStatus, int prev_ledStatus)
{
  if(curr_ledStatus != prev_ledStatus)
  {
    if(prev_ledStatus==0 || curr_ledStatus==0)
    {
      return 1;
    }
  }
  return 0;
}

void configureSensor(void)
{
  lightSensor.enableAutoRange(true);           
  lightSensor.setIntegrationTime(TSL2561_INTEGRATIONTIME_101MS);  // medium resolution and speed for best results according to datasheet
}

void selfSensorTest()
{
    Serial.println("Please Wait...");
    Serial.println("Testing Sensors and Ports");
    Serial.println("Leave all the Sensors and Ports untouched!");
    delay(2000);  //2sec wait time for all sensors to start 
    if (isnan(dht11.readHumidity()) || isnan(dht11.readTemperature()))
      Serial.println("Failed to read from DHT sensor!");
    else
      Serial.println("DHT sensor is working ");

    if(!lightSensor.begin())
      Serial.println(" no lightSensor2561 detected ... Check your circuit "); 
    else
      Serial.println("Light Sensor is working "); 
}
