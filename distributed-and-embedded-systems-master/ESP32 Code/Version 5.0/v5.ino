#include "SSD1306.h"
#include <SPI.h>
#include <SD.h>
#include <sd_defines.h>
#include <sd_diskio.h>
#include<DHT.h>
#include<Wire.h>
#include<Adafruit_Sensor.h>
#include<Adafruit_TSL2561_U.h>
#include <WiFi.h>
#include <IPAddress.h>
#include <WiFiType.h>
#include<WebServer.h>

//Definitions for DHT11
#define DHT_Pin  27      //DHT11 Pin Number
#define DHT_Type DHT11

//Definitions for Button
#define BUTTON_Pin 4

const int TIMEOUT = 3500; //in milliseconds
const char* SSID = "esp32";
const char* PASS = "MemeDream";
WebServer server(80);

//Definitions for RGB
#define R_Pin   5     //Pins for RGB Led
#define G_Pin   18
#define B_Pin   19
#define RGB_Type CC     // Common cathode led 
#define PIN_ON   HIGH
#define PIN_OFF  LOW


//Definitions for TSL2561
Adafruit_TSL2561_Unified lightSensor = Adafruit_TSL2561_Unified(TSL2561_ADDR_FLOAT, 12345);

DHT dht11(DHT_Pin, DHT_Type);

volatile float humidity[60], temp[60], lightLux[60];            //Store readings with a maximum of 60 readings ( 300/5 = 60)
unsigned long int printTime, readTime;
int interval = 10;
int intervalArr[] = {5, 10, 30, 60, 120, 300};          //All available intervals are stored in an array
int intervalPtr = 2, dataIndex = 0, maxIndex = 2;       //intervalPtr=1 for 10s, maxIndex=2 for 10s i.e 10s/5s
int currH_ledStatus = 0 , prevH_ledStatus = 0, currT_ledStatus = 0 , prevT_ledStatus = 0, currL_ledStatus = 0 , prevL_ledStatus = 0; //0-Green, 1-Amber, 2-Red

void selfSensorTest();
void readSensors();
void printData();
void configureSensor();

const int CS_PIN = 15;
const char * FILENAME = "condlog.txt";
unsigned long startTime;
unsigned long lightValue;
unsigned long serverTemp;
unsigned long serverHumidity;
unsigned long screenHumidity;
unsigned long screenTemp;
unsigned long screenLux;

SSD1306  display(0x3c, 21, 22);

void intervalConfig()
{
  maxIndex = interval / 5;
  dataIndex = 0;

  Serial.print("Output interval is now ");
  if (interval == 120 || interval == 300)
  {
    Serial.print(interval / 60);
    Serial.println("min");
  }
  else
  {
    Serial.print(interval);
    Serial.println("sec");
  }
}

void setup() {
  Serial.begin(115200);
  Serial.println("System Initialising...");
  dht11.begin();
  selfSensorTest();
  configureSensor();
  display.init();
  pinMode(R_Pin, OUTPUT);
  pinMode(G_Pin, OUTPUT);
  pinMode(B_Pin, OUTPUT);
  pinMode(BUTTON_Pin, INPUT);
  readTime = millis();            //To make start point of read and print interval same
  printTime = millis();           //To check print interval
  startTime = millis();
  digitalWrite(R_Pin, PIN_OFF);
  digitalWrite(G_Pin, PIN_ON);
  digitalWrite(B_Pin, PIN_OFF);
  intervalConfig();

  Serial.print("Connecting to ");
  Serial.println(SSID);
  WiFi.begin(SSID, PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(250);
    Serial.println(".");
  }
  Serial.print("Connected as :");
  Serial.println(WiFi.localIP());
  server.on("/", handle_OnConnect);
  server.onNotFound(handle_NotFound);
  server.begin();
  Serial.println("HTTP server started");
}


void loop() {
  if ((millis() - readTime) > 5000)       //Reads Data at 5s interval
  {
    readSensors();
    readTime = millis();
  }
  if ((millis() - printTime) > (interval * 1000)) //Prints Data at specified interval
  {
    printData();
    printTime = millis();
    displayData();
  }
  /*if(millis()-startTime>=120000){
    writeFile();
    startTime = millis();
    }*/
  if (digitalRead(BUTTON_Pin) == LOW)
  {
    delay(700);                               // interputs to avoid bouncing
    if (digitalRead(BUTTON_Pin) == LOW)
    {
      intervalPtr++;
      if (intervalPtr == 6) intervalPtr = 0;
      interval = intervalArr[intervalPtr];
      intervalConfig();

    }
  }
  server.handleClient();
}

void displayData(){

  String dispHum = "Temp: " + String(screenHumidity);
  String dispTemp = "Hum: " + String(screenTemp);
  String dispLux = "Lux: " + String(screenLux);
  display.clear();
  display.drawString(0,0, dispHum);
  display.drawString(0,10, dispTemp);
  display.drawString(0,20, dispLux);
  display.display();
  }

void readSensors()
{
  //Humidity Read and Status Check
  humidity[dataIndex] = dht11.readHumidity();
  serverHumidity = dht11.readHumidity();
  screenHumidity = dht11.readHumidity();
  if ((humidity[dataIndex] >= 50) && (humidity[dataIndex] <= 70))
    currH_ledStatus = 0;
  else if ((humidity[dataIndex] >= 30) && (humidity[dataIndex] < 50) || (humidity[dataIndex] > 70) && (humidity[dataIndex] <= 85))
    currH_ledStatus = 1;
  else if ((humidity[dataIndex] < 30) || (humidity[dataIndex] > 85))
    currH_ledStatus = 2;
  if (ledStatusCheck(currH_ledStatus, prevH_ledStatus))
  {
    changeLed(currH_ledStatus);
    Serial.println("\nHumidity in area has changed.");
  }
  delay(100);
  prevH_ledStatus = currH_ledStatus;

  //Temperature Read and Status Check
  temp[dataIndex] = dht11.readTemperature();
  serverTemp = dht11.readTemperature();
  screenTemp = dht11.readTemperature();
  if ((temp[dataIndex] >= 20) && (temp[dataIndex] <= 25))
    currT_ledStatus = 0;
  else if ((temp[dataIndex] >= 18) && (temp[dataIndex] < 20) || (temp[dataIndex] > 25) && (temp[dataIndex] <= 27))
    currT_ledStatus = 1;
  else if ((temp[dataIndex] < 18) || (temp[dataIndex] > 27))
    currT_ledStatus = 2;
  if (ledStatusCheck(currT_ledStatus, prevT_ledStatus))
  {
    changeLed(currT_ledStatus);
    Serial.println("\nTemperature in area has changed.");
  }
  delay(100);
  prevT_ledStatus = currT_ledStatus;

  //Intesity Read and Status Check
  sensors_event_t event;
  lightSensor.getEvent(&event);
  lightLux[dataIndex] = event.light;
  lightValue = event.light;
  screenLux = event.light;
  if ((lightLux[dataIndex] >= 60) && (lightLux[dataIndex] <= 1500))
    currL_ledStatus = 0;
  else if ((lightLux[dataIndex] >= 50) && (lightLux[dataIndex] < 60) || (lightLux[dataIndex] > 1500) && (lightLux[dataIndex] <= 2000))
    currL_ledStatus = 1;
  else if ((lightLux[dataIndex] < 50) || (lightLux[dataIndex] > 2000))
    currL_ledStatus = 2;
  if (ledStatusCheck(currL_ledStatus, prevL_ledStatus))
  {
    changeLed(currL_ledStatus);
    Serial.println("\nLight Intensity in area has changed.");
  }
  delay(100);
  prevL_ledStatus = currL_ledStatus;

  dataIndex++;
  if (dataIndex == maxIndex)
  {
    dataIndex = 0;
  }
}

void printData()
{
  for (int printIndex = 0; printIndex < maxIndex ; printIndex++)
  {
    Serial.print("Humidity: ");
    Serial.print(humidity[printIndex]);
    Serial.print(" %\t");
    Serial.print("Temperature: ");
    Serial.print(temp[printIndex]);
    Serial.print(" *C\t ");
    Serial.print("Lux: ");
    Serial.print(lightLux[printIndex]);
    Serial.println(" lux ");
  }
}

void changeLed(int ledStatus)
{
  if (ledStatus == 0)
  {
    digitalWrite(R_Pin, PIN_OFF);
    digitalWrite(G_Pin, PIN_ON);
    digitalWrite(B_Pin, PIN_OFF);
  }
  else if (ledStatus == 1)
  {
    digitalWrite(R_Pin, PIN_ON);
    digitalWrite(G_Pin, PIN_ON);
    digitalWrite(B_Pin, PIN_OFF);
  }
  else if (ledStatus == 2)
  {
    digitalWrite(R_Pin, PIN_ON);
    digitalWrite(G_Pin, PIN_OFF);
    digitalWrite(B_Pin, PIN_OFF);
  }
}

int ledStatusCheck(int curr_ledStatus, int prev_ledStatus)
{
  if (curr_ledStatus != prev_ledStatus)
  {
    if (prev_ledStatus == 0 || curr_ledStatus == 0)
    {
      return 1;
    }
    if (prev_ledStatus == 2 || curr_ledStatus == 2)
    {
      return 1;
    } if (prev_ledStatus == 1 || curr_ledStatus == 1)
    {
      return 1;
    }

  }
  return 0;
}

void configureSensor(void)
{
  lightSensor.enableAutoRange(true);            /* Auto-gain ... switches automatically between 1x and 16x */
  lightSensor.setIntegrationTime(TSL2561_INTEGRATIONTIME_101MS);  /* medium resolution and speed   */
}

void selfSensorTest()
{
  Serial.println("Please Wait...");
  Serial.println("Testing Sensors and Ports");
  Serial.println("Leave all the Sensors and Ports untouched!");
  delay(2000);  //2sec wait time for all sensors to start
  if (isnan(dht11.readHumidity()) || isnan(dht11.readTemperature()))
    Serial.println("Failed to read from DHT sensor!");
  else
    Serial.println("DHT sensor working correctly.");

  if (!lightSensor.begin())
    Serial.println("Failed to read from TSL2561");
  else
    Serial.println("Light Sensor working correctly.");
}

void writeFile() {
  SD.begin(CS_PIN);
  File file = SD.open(FILENAME, FILE_WRITE);
  if (file.available()) {
    file.println("");
    file.println("New Data Log: ");
    file.print("Humidity: ");
    file.print(dht11.readHumidity());
    file.print(" %\t");
    file.print("Temperature: ");
    file.print(dht11.readTemperature());
    file.print(" *C\t ");
    file.print("Lux: ");
    file.print(lightValue);
    file.println(" lux ");
    file.close();
  } else {
    Serial.println("File write failed.");
  }



  SD.end();
}
void handle_OnConnect()
{
  server.send(200, "text/html", SendHTML(serverTemp, serverHumidity, lightValue));
}

void handle_NotFound() {
  server.send(404, "text/plain", "Not found");
}

String SendHTML(long serverTemp, long serverHumidity, long lightValue) {
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr += "<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr += "<link href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,400,600\" rel=\"stylesheet\">\n";
  ptr += "<title>ESP32 Weather Report</title>\n";
  ptr += "<style>html { font-family: 'Open Sans', sans-serif; display: block; margin: 0px auto; text-align: center;color: #333333;}\n";
  ptr += "body{margin-top: 50px;}\n";
  ptr += "h1 {margin: 50px auto 30px;}\n";
  ptr += ".side-by-side{display: inline-block;vertical-align: middle;position: relative;}\n";
  ptr += ".humidity-icon{background-color: #3498db;width: 30px;height: 30px;border-radius: 50%;line-height: 36px;}\n";
  ptr += ".humidity-text{font-weight: 600;padding-left: 15px;font-size: 19px;width: 160px;text-align: left;}\n";
  ptr += ".humidity{font-weight: 300;font-size: 60px;color: #3498db;}\n";
  ptr += ".temperature-icon{background-color: #f39c12;width: 30px;height: 30px;border-radius: 50%;line-height: 40px;}\n";
  ptr += ".temperature-text{font-weight: 600;padding-left: 15px;font-size: 19px;width: 160px;text-align: left;}\n";
  ptr += ".temperature{font-weight: 300;font-size: 60px;color: #f39c12;}\n";
  ptr += ".superscript{font-size: 17px;font-weight: 600;position: absolute;right: -20px;top: 15px;}\n";
  ptr += ".data{padding: 10px;}\n";
  ptr += "</style>\n";
  ptr += "<script>\n";
  ptr += "setInterval(loadDoc,200);\n";
  ptr += "function loadDoc() {\n";
  ptr += "var xhttp = new XMLHttpRequest();\n";
  ptr += "xhttp.onreadystatechange = function() {\n";
  ptr += "if (this.readyState == 4 && this.status == 200) {\n";
  ptr += "document.getElementById(\"webpage\").innerHTML =this.responseText}\n";
  ptr += "};\n";
  ptr += "xhttp.open(\"GET\", \"/\", true);\n";
  ptr += "xhttp.send();\n";
  ptr += "}\n";
  ptr += "</script>\n";
  ptr += "</head>\n";
  ptr += "<body>\n";

  ptr += "<div id=\"webpage\">\n";

  ptr += "<h1>ESP32 Sensor Report</h1>\n";
  ptr += "<div class=\"data\">\n";
  ptr += "<div class=\"side-by-side temperature-icon\">\n";
  ptr += "<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n";
  ptr += "width=\"9.915px\" height=\"22px\" viewBox=\"0 0 9.915 22\" enable-background=\"new 0 0 9.915 22\" xml:space=\"preserve\">\n";
  ptr += "<path fill=\"#FFFFFF\" d=\"M3.498,0.53c0.377-0.331,0.877-0.501,1.374-0.527C5.697-0.04,6.522,0.421,6.924,1.142\n";
  ptr += "c0.237,0.399,0.315,0.871,0.311,1.33C7.229,5.856,7.245,9.24,7.227,12.625c1.019,0.539,1.855,1.424,2.301,2.491\n";
  ptr += "c0.491,1.163,0.518,2.514,0.062,3.693c-0.414,1.102-1.24,2.038-2.276,2.594c-1.056,0.583-2.331,0.743-3.501,0.463\n";
  ptr += "c-1.417-0.323-2.659-1.314-3.3-2.617C0.014,18.26-0.115,17.104,0.1,16.022c0.296-1.443,1.274-2.717,2.58-3.394\n";
  ptr += "c0.013-3.44,0-6.881,0.007-10.322C2.674,1.634,2.974,0.955,3.498,0.53z\"/>\n";
  ptr += "</svg>\n";
  ptr += "</div>\n";
  ptr += "<div class=\"side-by-side temperature-text\">Temperature</div>\n";
  ptr += "<div class=\"side-by-side temperature\">";
  ptr += (int)serverTemp;
  ptr += "<span class=\"superscript\">°C</span></div>\n";
  ptr += "</div>\n";
  ptr += "<div class=\"data\">\n";
  ptr += "<div class=\"side-by-side humidity-icon\">\n";
  ptr += "<svg version=\"1.1\" id=\"Layer_2\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n\"; width=\"12px\" height=\"17.955px\" viewBox=\"0 0 13 17.955\" enable-background=\"new 0 0 13 17.955\" xml:space=\"preserve\">\n";
  ptr += "<path fill=\"#FFFFFF\" d=\"M1.819,6.217C3.139,4.064,6.5,0,6.5,0s3.363,4.064,4.681,6.217c1.793,2.926,2.133,5.05,1.571,7.057\n";
  ptr += "c-0.438,1.574-2.264,4.681-6.252,4.681c-3.988,0-5.813-3.107-6.252-4.681C-0.313,11.267,0.026,9.143,1.819,6.217\"></path>\n";
  ptr += "</svg>\n";
  ptr += "</div>\n";
  ptr += "<div class=\"side-by-side humidity-text\">Humidity</div>\n";
  ptr += "<div class=\"side-by-side humidity\">";
  ptr += (int)serverHumidity;
  ptr += "<span class=\"superscript\">%</span></div>\n";
  ptr += "</div>\n";

  ptr += "<div class=\"data\">\n";
  ptr += "<div class=\"side-by-side humidity-icon\">\n";
  ptr += "<svg version=\"1.1\" id=\"Layer_2\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n\"; width=\"12px\" height=\"17.955px\" viewBox=\"0 0 13 17.955\" enable-background=\"new 0 0 13 17.955\" xml:space=\"preserve\">\n";
  ptr += "<path fill=\"#FFFFFF\" d=\"M1.819,6.217C3.139,4.064,6.5,0,6.5,0s3.363,4.064,4.681,6.217c1.793,2.926,2.133,5.05,1.571,7.057\n";
  ptr += "c-0.438,1.574-2.264,4.681-6.252,4.681c-3.988,0-5.813-3.107-6.252-4.681C-0.313,11.267,0.026,9.143,1.819,6.217\"></path>\n";
  ptr += "</svg>\n";
  ptr += "</div>\n";
  ptr += "<div class=\"side-by-side humidity-text\">Light Level</div>\n";
  ptr += "<div class=\"side-by-side humidity\">";
  ptr += (int)lightValue;
  ptr += "<span class=\"superscript\"> Lux</span></div>\n";
  ptr += "</div>\n";

  ptr += "</div>\n";
  ptr += "</body>\n";
  ptr += "</html>\n";
  return ptr;
}
